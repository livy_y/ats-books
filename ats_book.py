import torch
from TTS.api import TTS
import textract, re, os

OUTPUT_FOLDER = "output/"

NL_MODEL = "tts_models/nl/mai/tacotron2-DDC"
EN_MODEL_SLOW = "tts_models/en/ljspeech/glow-tts"
EN_MODEL_FAST = "tts_models/en/ljspeech/tacotron2-DDC"

# Get device
device = "cuda" if torch.cuda.is_available() else "cpu"

# First sentence of chapters
chapter_sentences = [
    "One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.",
    "It was not until it was getting dark that evening that Gregor awoke from his deep and coma-like sleep.",
    "No-one dared to remove the apple lodged in Gregors flesh, so it remained there as a visible reminder of his injury."]

chapter_start = ["This is the book Metamorphosis by Franz Kafka. Chapter one.", "Chapter two.", "Chapter three."]

### Prepare book ##
###################

# Get book
text = str(textract.process('demo.epub'))[106:]

# Remove weird garbage
text = re.sub(r'\\x\w{2}', '', text)

# Remove new senance
text = re.sub(r'\\n', ' ', text)

# Split chapters
chapters = []
try:
    for index, chapter_sentence in enumerate(chapter_sentences):
        parts = text.split(chapter_sentence)

        if index != 0:
            chapters.append(parts[0])
        text = chapter_start[index] + parts[1]
except IndexError:
    raise Exception("Problem with chapter " + str(chapter_sentence))

# Split chapters into chunks of 500 characters
splitted_chapters = []
character_limit = 500

for index, chapter in enumerate(chapters):
    splitted_chapters.append([])
    for i in range(0, len(chapter), character_limit):
        chunk = chapter[i:i + character_limit]
        splitted_chapters[index].append(chunk)

### TTS ###
###########

# Init TTS with the target model name
tts = TTS(model_name=EN_MODEL_FAST, progress_bar=True)

# Create output folder if it does not exist
if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)
    print(f"Directory '{OUTPUT_FOLDER}' created successfully.")
else:
    print(f"Directory '{OUTPUT_FOLDER}' already exists.")

# Converting each chapter
for chapter_index, chapter in enumerate(splitted_chapters):
    for chunk_index, chunk in enumerate(chapter):
        file_name = OUTPUT_FOLDER + "chapter " + str(chapter_index + 1) + ", part " + str(chunk_index + 1) + ".wav"
        print("\033[91mTranscribing to {}\033[0m".format(file_name))
        print("Transcribing the following text: " + chunk)
        tts.tts_to_file(text=chunk, file_path=file_name)

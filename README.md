# ATS-Books
ATS-books stands for "Automated Text-to-Speech for Books." The project aims to automate the process of converting text from books into spoken audio using Text-to-Speech (TTS) technology. By automating this process, users can easily listen to their favorite books or documents without having to read them manually. The name reflects the project's focus on providing an automated solution for converting books into audio format, making literature more accessible and convenient for users.
## Goal
This Python script aims to automate the process of converting text from a book (in EPUB format) into spoken audio using Text-to-Speech (TTS) technology. It utilizes pre-trained TTS models to generate high-quality audio from the book's text.
## How to Use
1. **Clone the Repository**: Clone this repository to your local machine using `git clone`.

2. **Install Dependencies**: Make sure you have Python installed on your machine. Install the required dependencies using pip.

3. **Prepare the Book**:
   - Place your EPUB file named `demo.epub` in the root directory of the repository.
   - Ensure the EPUB file contains the text of the book you want to convert to audio.

4. **Prepare python variables**: Make sure you change the python variables to the needs of your book.
   - **chapter_start**: Add the first sentence of a chapter here. eg. "We will now read chapter 1."
   - **chapter_sentences**: Add the first sentence of a chapter, so the software can recognise the start of the chapter.

5. **Run the Script**: Execute the Python script `tts_book.py` to start the conversion process.
   ```bash
   python tts_book.py
   ```

## Configuration
- Modify the NL_MODEL, EN_MODEL_SLOW, and EN_MODEL_FAST variables to specify the paths to the TTS models for different languages.
- Adjust the character_limit variable to control the maximum number of characters per chunk for splitting chapters.

## Notes
- This script currently supports EPUB format for input books.
- Ensure that the EPUB file does not contain any encrypted or DRM-protected content. 
- The script may require modifications to handle specific book formats or languages.

## License
This project is licensed under the GNU General Public License v3.0 (GPL-3.0).